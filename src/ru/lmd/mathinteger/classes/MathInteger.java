package ru.lmd.mathinteger.classes;

import ru.lmd.mathinteger.exception.OverflowException;
import ru.lmd.mathinteger.exception.PowerNegativeException;

/**
 * Класс MathInteger содержит методы для выполнения арифмитических операций.
 *
 * @author M. Lebedev 17IT18
 */
public class MathInteger {

    public static int outputTotal;

    /**
     * Возвращает результат сложения firstOperand и secondOperand
     *
     * @param firstOperand  первое слагаемое
     * @param secondOperand второе слагаемое
     * @return сумма
     * @throws OverflowException исключение из-за переполения int
     */
    public static int addition(int firstOperand, int secondOperand) throws OverflowException {

        int outputTest = firstOperand + secondOperand;
        if (((firstOperand ^ outputTest) & (secondOperand ^ outputTest)) < 0) {
            throw new OverflowException();
        } else {
            return outputTest;
        }
    }

    /**
     * Возвращает результат вычитания firstOperand и secondOperand
     *
     * @param firstOperand  уменьшаемое
     * @param secondOperand вычитаемое
     * @return разность
     * @throws OverflowException исключение из-за переполения int
     */
    public static int subtraction(int firstOperand, int secondOperand) throws OverflowException {

        int outputTest = firstOperand - secondOperand;
        if (((firstOperand ^ secondOperand) & (firstOperand ^ outputTest)) < 0) {
            throw new OverflowException();
        } else {
            return outputTest;
        }
    }

    /**
     * Возвращает результат произведения firstOperand на secondOperand
     *
     * @param firstOperand  множимое
     * @param secondOperand множитель
     * @return произведение
     * @throws OverflowException исключение из-за переполения int
     */
    public static int multiplication(int firstOperand, int secondOperand) throws OverflowException {

        if (firstOperand < 0 && secondOperand < 0) { //OverflowException срабатывает при умножение 2 отрицательных чисел
            return firstOperand * secondOperand;
        }

        int outputTest = firstOperand * secondOperand;
        if (((firstOperand ^ outputTest) & (secondOperand ^ outputTest)) < 0) {
            throw new OverflowException();
        } else {
            return outputTest;
        }
    }

    /**
     * Возвращает результат деления secondOperand на firstOperand
     *
     * @param firstOperand  делимое
     * @param secondOperand делитель
     * @return частное
     * @throws OverflowException исключение из-за переполения int
     */
    public static int division(int firstOperand, int secondOperand) throws OverflowException {

        if (firstOperand < 0 && secondOperand < 0) { //OverflowException срабатывает при деление 2 отрицательных чисел
            return firstOperand / secondOperand;
        }

        int outputTest = firstOperand / secondOperand;
        if (((firstOperand ^ outputTest) & (secondOperand ^ outputTest)) < 0) {
            throw new OverflowException();
        } else {
            return outputTest;
        }
    }

    /**
     * Возвращает результат возведения firstOperand в степень secondOperand
     *
     * @param firstOperand  основание
     * @param secondOperand показатель
     * @return результат возведения
     * @throws OverflowException      исключение из-за переполения int
     * @throws PowerNegativeException исключение из-за отрицательной степени
     */
    public static int raiseToThePower(int firstOperand, int secondOperand) throws PowerNegativeException, OverflowException {

        if (secondOperand == 0) {
            return 1;
        }

        if (firstOperand == 0) {
            return 0;
        }

        if (secondOperand < 0) {
            throw new PowerNegativeException();
        }

        outputTotal = 1;

        for (int i = 0; i < secondOperand; i++) {
            outputTotal *= firstOperand;
        }

        if (((firstOperand ^ outputTotal) & (secondOperand ^ outputTotal)) < 0) {
            throw new OverflowException();
        } else {
            return outputTotal;
        }
    }

    /**
     * Возвращает остаток от деления secondOperand на firstOperand
     *
     * @param firstOperand  делимое
     * @param secondOperand делитель
     * @return остаток
     * @throws OverflowException исключение из-за переполения int
     */
    public static int highlightTheRemainder(int firstOperand, int secondOperand) throws OverflowException {

        int outputTest = firstOperand * secondOperand;
        if (((firstOperand ^ outputTest) & (secondOperand ^ outputTest)) < 0) {
            throw new OverflowException();
        } else {
            return outputTest;
        }
    }
}