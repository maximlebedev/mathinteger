package ru.lmd.mathinteger.classes;

import ru.lmd.mathinteger.exception.*;
import ru.lmd.mathinteger.object.MathExample;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для подсчёта примера введённого с консоли
 *
 * @author M. Lebedev 17IT18
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("input:");
        try {
            Scanner scanner = new Scanner(System.in);
            String inputData = scanner.nextLine();
            if (isCorrect(inputData)) ;
            MathExample mathExample = new MathExample();
            mathExample = parsing(inputData, mathExample);
            int outputTotal = backTotal(mathExample.sign, mathExample.firstOperand, mathExample.secondOperand);
            System.out.println("\noutput:\n" + outputTotal);
        } catch (DataException e) {
            System.err.println("Incorrect data.");
        } catch (OverflowException e) {
            System.err.println("Integer overflow.");
        } catch (PowerNegativeException e) {
            System.err.println("Negative power.");
        } catch (ArithmeticException e) {
            System.err.println("Division on 0.");
        } catch (NumberFormatException e) {
            System.err.println("One of the numbers is greater than int.");
        }
    }

    /**
     * Метод проверки на корректность введённых данных с консоли
     *
     * @param inputData пример
     * @throws DataException исключение из-за некорректных данных
     */
    public static boolean isCorrect(String inputData) throws DataException {
        Pattern p = Pattern.compile("[+-]?[0-9]+\\s[+-\\/*\\^%]\\s[+-]?[0-9]+");
        Matcher m = p.matcher(inputData);

        if (m.matches() != true) {
            throw new DataException();
        }

        return true;
    }

    /**
     * Метод заполняет поля объекта
     *
     * @param inputData   входные данные с консоли
     * @param mathExample объект
     * @return объект с заполненными полями
     */
    public static MathExample parsing(String inputData, MathExample mathExample) {
        String[] array;
        array = inputData.split(" ");

        mathExample.firstOperand = Integer.parseInt(array[0]);
        mathExample.secondOperand = Integer.parseInt(array[2]);
        mathExample.sign = array[1].charAt(0);

        return mathExample;
    }

    /**
     * Метод содержит switch, который вызывает методы из класса MathInteger, и возвращает результат
     *
     * @param sign          знак между операндами
     * @param firstOperand  первый операнд
     * @param secondOperand второй операнд
     * @return результат вычисления
     * @throws OverflowException      исключение переполнение int
     * @throws PowerNegativeException исключение отрицательная степень при возведение в степень
     */
    public static int backTotal(char sign, int firstOperand, int secondOperand) throws OverflowException, PowerNegativeException {
        int outputTotal = 0;
        switch (sign) {
            case '+':
                outputTotal = MathInteger.addition(firstOperand, secondOperand);
                break;
            case '-':
                outputTotal = MathInteger.subtraction(firstOperand, secondOperand);
                break;
            case '*':
                outputTotal = MathInteger.multiplication(firstOperand, secondOperand);
                break;
            case '/':
                outputTotal = MathInteger.division(firstOperand, secondOperand);
                break;
            case '^':
                outputTotal = MathInteger.raiseToThePower(firstOperand, secondOperand);
                break;
            case '%':
                outputTotal = MathInteger.highlightTheRemainder(firstOperand, secondOperand);
                break;
        }

        return outputTotal;
    }
}