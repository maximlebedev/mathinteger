import org.junit.Test;
import ru.lmd.mathinteger.classes.MathInteger;
import ru.lmd.mathinteger.exception.*;

import static junit.framework.TestCase.assertEquals;

class MathIntegerExceptionTest {

    @Test(expected = ArithmeticException.class)
    public void additionOverflow() throws OverflowException {
        int a = 2147483647;
        int b = 5;
        int result = MathInteger.addition(a, b);
        assertEquals(a + b, result);
    }

    @Test(expected = ArithmeticException.class)
    public void multiplyOverflow() throws OverflowException {
        int a = 2147483647;
        int b = 2;
        int result = MathInteger.multiplication(a, b);
        assertEquals(a * b, result);
    }

    @Test(expected = ArithmeticException.class)
    public void powOverFlow() throws OverflowException, PowerNegativeException {
        int a = 50 + (int) (Math.random() * 100);
        int b = 50 + (int) (Math.random() * 100);
        int result = MathInteger.raiseToThePower(a, b);
        assertEquals((int) Math.pow(a, b), result);
    }

    @Test(expected = ArithmeticException.class)
    void divisionOnZero() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = 0;
        int result = MathInteger.division(a, b);
        assertEquals(a / b, result);
    }

    @Test(expected = ArithmeticException.class)
    public void powOnZero() throws OverflowException, PowerNegativeException {
        int a = 1 + (int) (Math.random() * 100);
        int b = 0;
        int result = MathInteger.raiseToThePower(a, b);
        assertEquals(a % b, result);
    }

    @Test(expected = ArithmeticException.class)
    public void powOnNegative() throws OverflowException, PowerNegativeException {
        int a = 1 + (int) (Math.random() * 10);
        int b = -1 + (int) (Math.random() * -10);
        int result = MathInteger.raiseToThePower(a, b);
        assertEquals((int) Math.pow(a, b), result);
    }
}