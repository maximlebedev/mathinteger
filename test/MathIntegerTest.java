import org.junit.jupiter.api.Test;
import ru.lmd.mathinteger.classes.MathInteger;
import ru.lmd.mathinteger.exception.OverflowException;
import ru.lmd.mathinteger.exception.PowerNegativeException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MathIntegerTest {

    @Test
    void additionPositiveWithPositive() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a + b, MathInteger.addition(a, b));
    }

    @Test
    void additionPositiveWithNegative() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a + b, MathInteger.addition(a, b));
    }

    @Test
    void additionNegativeWithPositive() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a + b, MathInteger.addition(a, b));
    }

    @Test
    void additionNegativeWithNegative() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a + b, MathInteger.addition(a, b));
    }

    @Test
    void substractionPositiveWithPositive() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a - b, MathInteger.subtraction(a, b));
    }

    @Test
    void substractionPositiveWithNegative() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a - b, MathInteger.subtraction(a, b));
    }

    @Test
    void substractionNegativeWithPositive() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a - b, MathInteger.subtraction(a, b));
    }

    @Test
    void substractionNegativeWithNegative() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a - b, MathInteger.subtraction(a, b));
    }

    @Test
    void multiplicationPositiveWithPositive() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, MathInteger.multiplication(a, b));
    }

    @Test
    void multiplicationPositiveWithNegative() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a * b, MathInteger.multiplication(a, b));
    }

    @Test
    void multiplicationNegativeWithPositive() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, MathInteger.multiplication(a, b));
    }

    @Test
    void multiplicationNegativeWithNegative() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a * b, MathInteger.multiplication(a, b));
    }

    @Test
    void divisionPositiveWithPositive() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a / b, MathInteger.division(a, b));
    }

    @Test
    void divisionPositiveWithNegative() throws OverflowException {
        int a = 1 + (int) (Math.random() * 100);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a / b, MathInteger.division(a, b));
    }

    @Test
    void divisionNegativeWithPositive() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a / b, MathInteger.division(a, b));
    }

    @Test
    void divisionNegativeWithNegative() throws OverflowException {
        int a = -100 + (int) (Math.random() * 99);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a / b, MathInteger.division(a, b));
    }

    @Test
    void powPositiveWithPositive() throws OverflowException, PowerNegativeException {
        int a = 5;
        int b = 5;
        assertEquals(Math.pow(a, b), MathInteger.raiseToThePower(a, b));
    }

    @Test
    void powNegativeWithPositive() throws OverflowException, PowerNegativeException {
        int a = -5;
        int b = 5;
        assertEquals(Math.pow(a, b), MathInteger.raiseToThePower(a, b));
    }

    @Test
    void powNegativeWithNegative() throws PowerNegativeException, OverflowException {
        int a = 0;
        int b = -5;
        assertEquals(0, MathInteger.raiseToThePower(a, b));
    }

    @Test
    void powZeroWithPositive() throws PowerNegativeException, OverflowException {
        int a = 0;
        int b = 5;
        assertEquals(Math.pow(a, b), MathInteger.raiseToThePower(a, b));
    }

    @Test
    void powZeroWithNegative() throws OverflowException, PowerNegativeException {
        int a = 0;
        int b = -5;
        assertEquals(0, MathInteger.raiseToThePower(a, b));
    }


    @Test
    void powZeroWithZero() throws OverflowException, PowerNegativeException {
        int a = 0;
        int b = 0;
        assertEquals(Math.pow(a, b), MathInteger.raiseToThePower(a, b));
    }

    @Test
    void powPositiveWithNegative() {

    }
}